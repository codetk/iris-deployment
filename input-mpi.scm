(dir "/home/users/tkondic/projects/vortex-disruption/vtextdist")
(name "trial_mpi")
(settings-key iris-mpi-debug)
(storage-type ptr)

(distro
 (type mpi))

(fft (lib fftw3))


(grid (basis fourier)
      (res 1022 1024)
      
      (scalef (type constant)
	      (len "2*pi" "2*pi")))


(physics
 (type vortex-disruption)
 (ic
  (type zero-circulation)
  (norm 1))
 (parameters
  (nu 0.001)
  (eta 0.001)
  (B0 1e-7)
  (Rv 1))
 (data
  (data-init? #t)
  (state 1)
  (ts 0.1)
  (samba 1)))





(evolve
 (step
  (type etdrk4)
  (control dt-is-const)))


(linalg
 (type diagonal))

(time
 (range 0 100)
 (dt-start 1e-4)
 (segment 5e-2))
