#!/bin/bash -l

#SBATCH -J vtexdist
#SBATCH -n 256
#SBATCH -c 1
#SBATCH --time=0-12:00:00
#SBATCH -p batch
#SBATCH --qos=qos-batch
#SBATCH -C skylake
ARCTIC_SETTINGS_DIR=/home/users/tkondic/projects/vortex-disruption
export ARCTIC_SETTINGS_DIR
WORK_DIR="$SLURM_SUBMIT_DIR/vtexdist"
module load toolchain/intel
module load numlib/FFTW/3.3.8-intel-2019a
$HOME/.local/opt/gnu/bin/guile --no-auto-compile -l "$ARCTIC_SETTINGS_DIR/runme.scm"
cd "/home/users/tkondic/projects/vortex-disruption/vtextdist"
cd __code
make
make run

