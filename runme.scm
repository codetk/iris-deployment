(define arctic-dir "/home/users/tkondic/arctic-nextgen")
(define schemetran-dir "/home/users/tkondic/schemetran/src")
(define settings-dir (getenv "ARCTIC_SETTINGS_DIR"))
(add-to-load-path schemetran-dir)
(add-to-load-path (string-append arctic-dir "/engine"))
(add-to-load-path (string-append arctic-dir "/user"))


(use-modules (arctic cmd)
	     ((arctic action gen-input) #:prefix gen-i-)
	     ((arctic action start) #:prefix start-)
	     ((arctic action distro) #:prefix distro-)
	     ((arctic action io) #:prefix io-)
	     ((arctic action storage) #:prefix storage-)
	     ((arctic action fft) #:prefix fft-)
	     ((arctic action linalg) #:prefix linalg-)
	     ((arctic action basis aux) #:prefix aux-)
	     ((arctic action basis domain) #:prefix domain-)
	     ((arctic action basis deriv) #:prefix deriv-)
	     ((arctic action compilation) #:prefix comp-)
	     ((arctic action evolve) #:prefix evolve-)
	     ((arctic action shape) #:prefix shape-)
	     ((arctic action slice-distro) #:prefix slice-)
	     (schemetran)
	     (schemetran util))

(define (run-fft input)
  (fft-action
   (slice-action
    (storage-action-prolog
     (shape-action
      (fft-setup
       (domain-domain-input
	(io-action
	 (distro-action
	  (gen-i-action
	   (start-action input)))))))))))
  

(define (run-algebra input)
  (deriv-action
   (domain-action
    (linalg-action
     (aux-action input)))))

(define (run-evolve input)
  (evolve-action input))


(define (run-all input)
  (storage-action
   (run-evolve
    (run-algebra
     (run-fft input)))))


  

;; (define res (comp-prepare (with-inputs run-all "input.scm" "settings.scm")))
(define res-mpi (comp-prepare (with-inputs run-all
					   (string-append settings-dir "/input-mpi.scm")
					   (string-append settings-dir "/settings.scm"))))
